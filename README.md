# XDebug in Docker with Visual Studio Code on Linux

This is the most stripped down setup I could come up with to try and test XDebug executing in a Docker container and communicating with the PHP Debug Visual Studio Code Plugin by Felix Becker.

## Requirements

- Docker version 19.03.6, build 369ce74a3c or later
- docker-compose version 1.25.0, build 0a186604 or later
- Visual Studio Code version 1.42 or later
- VSCode Extension PHP Debug (by Felix Becker) 1.13.0 or later

## How To

- `docker-compose up -d`
- Open `index.php`
- Set some break points 
- Click on the debug button in the VSCode activity bar
- Click `Launch currently open script` next to the green arrow at the top

## Issues

- `Listen for XDebug` does nothing opening the page in a browser